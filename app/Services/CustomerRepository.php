<?php


namespace App\Services;

/**
 * The CustomerRepository class
 * @package App\Services
 */
class CustomerRepository
{
    /**
     * Constant to identify the next customer
     * @var string
     */
    public const NEXT = 'next';

    /**
     * Constant to identify the previous customer
     * @var string
     */
    public const PREV = 'prev';

    /**
     * The customer data config path
     * @var string
     */
    private const CUSTOMERS_PATH = 'data.customers';

    /**
     * Find a customer by its ID
     * @param string $id
     * @return array|null
     */
    public function findById(string $id): ?array
    {
        $allCustomers = config(self::CUSTOMERS_PATH);
        return $allCustomers[$id] ?? null;
    }

    /**
     * Find the next or the previous customer for a specific customer
     * @param string $id
     * @param string $type
     * @return array|null
     */
    public function findAdjacent(string $id, string $type): ?array
    {
        // Get all customers
        $allCustomers = config(self::CUSTOMERS_PATH);
        if ($type === self::PREV) {
            $allCustomers = array_reverse($allCustomers);
        }

        // Find the adjacent one
        $found = false;
        foreach ($allCustomers as $idCustomer => $customer) {
            if ($found) {
                return $customer;
            }

            if ($idCustomer === $id) {
                $found = true;
            }
        }

        // If not found return null
        if ($found === false) {
            return null;
        }

        return reset($allCustomers);
    }

    /**
     * Finds all customers of a category
     * @param string $identifier
     * @return array
     */
    public function findByCategory(string $identifier): array
    {
        $allCustomers = config(self::CUSTOMERS_PATH);
        $customers = array_filter(
            $allCustomers,
            function (array $item) use ($identifier): bool {
                if (!isset($item['categories'])) {
                    return false;
                }
                return in_array($identifier, $item['categories']);
            }
        );

        return $customers;
    }
}
