@extends('layouts.main', ['bodyClass' => 'customer'])

@section('title', $customer['name'])

@section('content')

    <div class="customer-container js-customer-container">

        @php ($copyContent = $customer['content'])
        @foreach ($customer['content'] as $key => $item)
            @if ($item['type'] === 'image')
                <div class="customer-item image js-customer-item {{ $key === 0 ? 'js-current' : '' }}"
                     style="background-image: url({{ $item['path'] }}); background-color: {{ $item['backgroundColor'] ?? '#000000' }}"
                     data-id="{{ $key }}">

                    @if ($key === 0)
                        <div class="customer-nav">
                            @foreach ($customer['content'] as $navKey => $navItem)
                                @if ($navItem['displayNav'])
                                    <a href="#" class="customer-nav-item js-customer-nav-item" data-target="{{ $navKey }}">{{ $navItem['name'] }}</a>
                                @endif
                            @endforeach
                            <div class="nav-next js-customer-nav-item" data-target="1"></div>
                        </div>
                    @endif
                </div>
            @elseif ($item['type'] === 'slider')
                <div class="customer-item slider js-customer-item {{ $key === 0 ? 'js-current' : '' }} js-slider"
                     style="background-color: {{ $item['backgroundColor'] ?? '#000000' }}"
                     data-id="{{ $key }}">

                    <div class="slider-progress">
                        <div class="slider-bar js-slider-bar"></div>
                    </div>

                    @foreach ($item['images'] as $imagesKey => $image)
                        <img src="{{ $item['path'] . $image }}"
                             class="slider-item js-slider-item"
                             alt="{{ $customer['name'] }} - {{ $item['name'] }} {{ $imagesKey + 1 }}">
                    @endforeach
                </div>
            @elseif ($item['type'] === 'video')
                <div class="customer-item video js-video js-customer-item {{ $key === 0 ? 'js-current' : '' }}"
                     style="background-color: {{ $item['backgroundColor'] ?? '#000000' }}"
                     data-id="{{ $key }}">
                    <video controls preload="auto" poster="{{ $item['poster'] }}">
                        <source src="{{ $item['path'] }}" type="video/mp4">
                    </video>
                </div>
            @endif
        @endforeach
    </div>

    <div class="customer-nav">
        <a href="{{ route('customer', ['id' => $prev['id']]) }}"><span class="arrow">&lt;</span> Précédent</a>
        <a href="{{ route('home') }}1">Accueil</a>
        <a href="{{ route('customer', ['id' => $next['id']]) }}">Suivant <span class="arrow">&gt;</span></a>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.customer.init();
        });
    </script>
@endpush
