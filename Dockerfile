FROM ubuntu:18.04
MAINTAINER Angelo Mahrouchi

ARG DEBIAN_FRONTEND=noninteractive

# Init
RUN apt-get update && apt-get upgrade -y

# PHP 7 repo
RUN apt-get -y install software-properties-common
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update

# Install apache php 7
RUN apt-get install  -y --allow-unauthenticated \
	apache2 libapache2-mod-php7.4 \
	php7.4 php7.4-bz2 php7.4-cgi php7.4-cli php7.4-common php7.4-curl php7.4-dev \
	php7.4-mbstring php7.4-fpm php7.4-gd php7.4-gmp php7.4-imap php7.4-intl \
	php7.4-json php7.4-mysql php7.4-opcache \
	php7.4-phpdbg php7.4-snmp php7.4-sybase \
	php7.4-tidy php7.4-xdebug php7.4-xmlrpc php7.4-xsl php7.4-zip

# Disable xdebug for cli
# RUN phpdismod -v 7.4 -s cli xdebug

# Install composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer

# Install tools
RUN apt-get install -y vim git sendmail
RUN apt-get install -y icu-devtools icu-doc libicu-dev
RUN apt-get install -y iputils-ping

# Apache configuration
RUN rm /etc/apache2/sites-enabled/*
COPY ./vhosts/*.conf /etc/apache2/sites-available/
RUN a2ensite site.conf
RUN a2enmod rewrite
RUN a2enmod headers

# PHP configuration
COPY ./php/php.ini /etc/php/7.4/apache2
COPY ./php/php.ini /etc/php/7.4/cli

# Server name for CLI debugging (xDebug)
ENV PHP_IDE_CONFIG serverName=localhost

WORKDIR /var/www/app

EXPOSE 80 22 443

# CMD ["/bin/bash"]
