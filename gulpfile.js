'use strict';

let gulp   = require('gulp'),
    less   = require('gulp-less'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minCss = require('gulp-minify-css'), // use gulp-clean-css
    path   = require('path');


// Build app CSS
gulp.task('app:less', function () {
    return gulp.src(['./app/assets/less/**/*.less'])
               .pipe(less({
                   paths : [path.join(__dirname)]
               }))
               .pipe(concat('app.css'))
               .pipe(minCss())
               .pipe(gulp.dest('./app/public/assets'));
});

// Build app JS
gulp.task('app:js', function () {
    return gulp.src('./app/assets/js/**/*.js')
               .pipe(concat('app.js'))
               .pipe(uglify())
               .pipe(gulp.dest('./app/public/assets'));
});

// Default task
gulp.task('watch', ['app:less', 'app:js'], function () {
    gulp.watch(
        [
            './src/js/**/*.js',
            './src/scss/**/*.scss'
        ],
        ['app:less', 'app:js']
    );
});

// Default task
gulp.task('default', [
    'app:less',
    'app:js',
]);
