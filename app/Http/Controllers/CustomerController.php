<?php

namespace App\Http\Controllers;

use App\Services\CustomerRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Controller handling the customer pages
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    /**
     * View a customer page
     * @param string             $id
     * @param CustomerRepository $customerRepository
     * @return View
     */
    public function index(
        string $id,
        CustomerRepository $customerRepository
    ): View
    {
        // Retrieve the customer
        $customer = $customerRepository->findById($id);
        if (empty($customer) || empty($customer['content'])) {
            abort(404);
        }

        $next = $customerRepository->findAdjacent($id, CustomerRepository::NEXT);
        $prev = $customerRepository->findAdjacent($id, CustomerRepository::PREV);
        if (empty($next) || empty($prev)) {
            abort(400);
        }

        return view('customer', [
            'customer' => $customer,
            'next'     => $next,
            'prev'     => $prev,
        ]);
    }
}
