var mzc = mzc || {};

/**
 * Category page JS module
 * @type {{}}
 */
mzc.category = {
    /**
     * The nav items jQuery elements
     * @type {jQuery}
     */
    $navItems : $('.js-nav-item'),

    /**
     * The customer list jQuery element
     * @type {jQuery}
     */
    $categoryCustomerList : $('.js-category-customer-list'),

    /**
     * The category items jQuery elements
     * @type {jQuery}
     */
    $categoryItem : $('.js-category-item'),

    /**
     * Customer image items
     * @type {jQuery}
     */
    $customerImg : $('.js-customer-image'),

    /**
     * Customer description content items
     * @type {jQuery}
     */
    $customerDescriptionContent : $('.js-customer-description-content'),

    /**
     * The logo slider container
     * @type {jQuery}
     */
    $logoSlider : $('.js-logo-slider'),

    /**
     * Drag zone for the slider
     * @type {jQuery}
     */
    $logoSliderDrag : $('.js-logo-slider-drag'),

    /**
     * Slider container toggle button
     * @type {jQuery}
     */
    $logoSliderToggle : $('.js-logos-toggle'),

    /**
     * The carousel current position
     * @type {int}
     */
    carouselPosition : 0,

    /**
     * Module initializer
     * @return {void}
     */
    init : function () {

        // Init navigation
        this.$navItems.click(this.navigateCustomers.bind(this));

        // See more
        this.$customerImg.mouseover(function (e) {
            this.$customerDescriptionContent.addClass('show-see-more');
        }.bind(this));

        this.$customerImg.mouseout(function (e) {
            this.$customerDescriptionContent.removeClass('show-see-more');
        }.bind(this));

        if (this.$categoryItem.length === 1) {
            this.$navItems.hide();
        } else {
            this.$navItems.filter('.prev').hide();
        }

        this.initLogoSlider();
    },

    /**
     * Initializes the logo slider
     * @return {void}
     */
    initLogoSlider : function () {
        var initialCursorPosition = 0;

        // Init cursor position
        this.$logoSlider.on('dragstart', function (e) {
            // Hide the dragged element "ghost"
            var emptyImage = document.createElement('img');
            emptyImage.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
            e.originalEvent.dataTransfer.setDragImage(emptyImage, 0, 0);

            // Store the initial position
            initialCursorPosition = e.pageX;
        });

        // Set slider scroll
        this.$logoSlider.on('drag', function (e) {
            var currCursorPosition = e.pageX;
            if (currCursorPosition !== 0) {
                var scroll       = this.$logoSlider.scrollLeft()
                let cursorOffset = initialCursorPosition - currCursorPosition;
                this.$logoSlider.scrollLeft(scroll + cursorOffset);
            }
        }.bind(this));

        // Toggle
        this.$logoSliderToggle.click(function (e) {
            e.preventDefault();
            this.$logoSlider.toggleClass('show');
        }.bind(this))
    },

    /**
     * Navigate through the customers
     * @param {Event} e
     * @return {void}
     */
    navigateCustomers : function (e) {
        // Block the navigation for the 1st and the last items
        if (
            (this.carouselPosition === 0 && $(e.currentTarget).is('.prev'))
            || (
                this.carouselPosition === this.$categoryItem.length - 1
                && $(e.currentTarget).is('.next')
            )
        ) {
            return;
        }

        // Move the customer container
        $(e.currentTarget).is('.next') ? this.carouselPosition++ : this.carouselPosition--;
        this.$categoryCustomerList.css('left', (this.carouselPosition * -100) + 'vw');

        // Show navigation buttons
        this.$navItems.show();
        if (this.carouselPosition === this.$categoryItem.length - 1) {
            this.$navItems.filter('.next').hide();
        }
        if (this.carouselPosition === 0) {
            this.$navItems.filter('.prev').hide();
        }

    }
};
