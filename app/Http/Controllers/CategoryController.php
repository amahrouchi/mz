<?php

namespace App\Http\Controllers;

use App\Services\CategoryRepository;
use App\Services\CustomerRepository;
use Illuminate\View\View;

/**
 * Controller handling the categories of work
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * View a category page
     * @param string             $id
     * @param CustomerRepository $customerRepository
     * @param CategoryRepository $categoryRepository
     * @return View
     */
    public function listCustomers(
        string $id,
        CustomerRepository $customerRepository,
        CategoryRepository $categoryRepository
    ): View
    {
        // Retrieve the category
        $category = $categoryRepository->findById($id);
        if (empty($category)) {
            abort(404);
        }

        // Retrieve the customers
        $customers = $customerRepository->findByCategory($id);
        if (empty($customers)) {
            abort(404);
        }

        // Gather all categories
        $allCategories = $categoryRepository->findAll();

        return view('category.list-customers', [
            'allCategories' => $allCategories,
            'category'      => $category,
            'customers'     => $customers,
        ]);
    }

    /**
     * Displays the video category page
     * @param CategoryRepository $categoryRepository
     * @return View
     */
    public function videos(CategoryRepository $categoryRepository): View
    {
        // Retrieve the category
        $category = $categoryRepository->findById(CategoryRepository::VIDEO_CATEGORY_ID);
        if (empty($category)) {
            abort(404);
        }

        // Gather all categories
        $allCategories = $categoryRepository->findAll();
        $videos        = $categoryRepository->findAllVideos();

        return view('category.videos', [
            'allCategories'        => $allCategories,
            'category'             => $category,
            'type'                 => CategoryRepository::VIDEO_CATEGORY_ID,
            'liveSubType'          => CategoryRepository::VIDEO_SUBTYPE_LIVE,
            'institutionalSubType' => CategoryRepository::VIDEO_SUBTYPE_INSTITUTIONAL,
            'clipSubType'          => CategoryRepository::VIDEO_SUBTYPE_CLIP,
            'videos'               => $videos,
        ]);
    }

    /**
     * Displays the photo category page
     * @param CategoryRepository $categoryRepository
     * @return View
     */
    public function photos(CategoryRepository $categoryRepository): View
    {
        // Retrieve the category
        $category = $categoryRepository->findById(CategoryRepository::PHOTO_CATEGORY_ID);
        if (empty($category)) {
            abort(404);
        }

        // Gather all categories
        $allCategories = $categoryRepository->findAll();

        return view('category.in-progress', [
            'allCategories' => $allCategories,
            'category'      => $category,
            'type'          => CategoryRepository::VIDEO_CATEGORY_ID,
        ]);
    }
}
