<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

        <title>Hippograph - @yield('title')</title>
    </head>
    <body class="{{ $bodyClass ?? ''}}">

        @include('layouts.menu.menu', ['categories' => $categories])

        @yield('content')

        @include('partial.contact')

        <script type="text/javascript" src="{{ mix('/js/vendor.js') }}"></script>
        <script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
        @stack('scripts')

    </body>
</html>


