<div class="category-header">
    <div class="category-header-content">
        <nav class="category-list">
            <ul>
                <li>
                    <a href="{{ route('home') }}">Accueil</a>
                </li>

                @foreach ($allCategories as $currCategory)
                    @continue ($currCategory['id'] === $category['id'])
                    <li>
                        <a href="{{ route('category', ['id' => $currCategory['id']]) }}">{{ $currCategory['name'] }}</a>
                    </li>
                @endforeach
            </ul>
        </nav>
        <div class="logo">
            <a href="{{ route('home') }}">
                <img src="/assets/img/logo_chrome.png" alt="Logo chrome">
            </a>
        </div>
        <h1 class="{{ $type ?? '' }}" style="border-color : {{ $category['backgroundColor'] }}">
            {!! $category['htmlTitle'] !!}
        </h1>
    </div>

    <h2 class="category-description" style="border-top-color : {{ $category['backgroundColor'] }}">
        {{ $category['description'] }}
    </h2>
</div>

