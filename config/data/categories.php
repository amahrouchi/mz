<?php
/**
 * Lists the type of works done for clients
 */

return [
    'identite-visuelle' => [
        'id'              => 'identite-visuelle',
        'name'            => 'Identité visuelle',
        'htmlTitle'       => 'iDeNtITE<br> VisuElLE',
        'description'     => 'Conception de logos et chartes graphiques',
        'backgroundColor' => '#20F5EF',
    ],
    'videos'     => [
        'id'              => 'videos',
        'name'            => 'Videos',
        'htmlTitle'       => 'VIDeos',
        'description'     => 'Réalisation de films publicitaires, clips, films institutionnels, motion design, VFX',
        'backgroundColor' => '#FF2C25',
    ],
    'illustrations'      => [
        'id'              => 'illustrations',
        'name'            => 'Illustrations',
        'htmlTitle'       => 'Illustrations',
        'description'     => 'Description de la categorie',
        'backgroundColor' => '#20F5EF',
    ],
    'photos'            => [
        'id'              => 'photos',
        'name'            => 'Photos',
        'htmlTitle'       => 'Photos',
        'description'     => 'Photos',
        'backgroundColor' => '#20F5EF',
    ],
];
