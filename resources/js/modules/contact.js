var mzc = mzc || {};

/**
 * Main menu JS script
 * @type {{}}
 */
mzc.contact = {

    /**
     * The contact container element
     * @type jQuery
     */
    $contactContainer : null,

    /**
     * The contact toggle button
     * @type jQuery
     */
    $contactToggle : null,

    /**
     * Init the JS module
     * @return {void}
     */
    init : function () {
        this.$contactContainer = $('.js-contact-container');
        this.$contactToggle    = $('.js-contact-toggle');
        this.$contactToggle.click(this.toggleContact.bind(this));
    },

    /**
     * Toggles the contact container visibility
     * @param {Event} e
     * @return {void}
     */
    toggleContact : function (e) {
        e.preventDefault();
        this.$contactContainer.toggleClass('visible');
    }
};
