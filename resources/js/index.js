var mzc = mzc || {};

/**
 * Index page script
 * @type {{}}
 */
mzc.index = {
    /**
     * The Window jQuery object
     * @type {jQuery}
     */
    $window : $(window),

    /**
     * The video player jQuery element
     * @type {jQuery}
     */
    $videoPlayer : $('.js-video-player'),

    /**
     * The jQuery elements used to close the video
     * @type {jQuery}
     */
    $stopVideo : $('.js-stop-video'),

    /**
     * Button used to unmute the intro video
     * @type {jQuery}
     */
    $toggleMute : $('.js-toggle-mute'),

    /**
     * The home container jQuery element
     * @type {jQuery}
     */
    $homeContainer : $('.js-home-container'),

    /**
     * The home menu jQuery element
     * @type {jQuery}
     */
    $homeMenu : $('.js-home-menu'),

    /**
     * The screen container element
     * @type {jQuery}
     */
    $screenContainer : $('.js-screen-container'),

    /**
     * The interval resizing the window at the beginning of the page life
     * @type {int|null}
     */
    resizeInterval : null,

    /**
     * Module initializer
     * @return {void}
     */
    init : function () {
        // Center the video tag
        this.$window.resize(this.centerVideo.bind(this));
        this.initResize();

        // Hides the player
        this.$videoPlayer.on('ended', this.hideVideo.bind(this, 2000));
        this.$stopVideo.click(this.hideVideo.bind(this, 0));
        this.$toggleMute.click(this.toggleIntroMute.bind(this));

        // Video visible: manual video autoplay (desktop/tablet)
        if (this.$videoPlayer.is(':visible')) {
            this.$videoPlayer[0].play();
        }
        // Else: show the menu (phone)
        else {
            this.$homeMenu.addClass('menu-visible');
            this.$homeContainer.show();
        }
    },

    /**
     * Launches a window.resize event every 200ms during 2 seconds at the loading of the page
     * because Chrome seems to trigger the event to early on desktop viewport size sometimes
     * and the intro video is not centered correctly... weird...
     * @return {void}
     */
    initResize : function () {
        this.resizeInterval = setInterval(this.centerVideo.bind(null, this.$videoPlayer, this.$window), 200);

        // Disable the 5 sec timeout because when the video 1st loads
        // sometimes it takes more than 2 sec and the video is not centered
        setTimeout(function () {
            clearInterval(this.resizeInterval);
        }.bind(this), 5000);
    },

    /**
     * Center the video
     * @return {void}
     */
    centerVideo : function ($videoPlayer, $container) {

        // Player and window ratios
        var playerRatio = $videoPlayer.width() / $videoPlayer.height(),
            windowRatio = $container.width() / $container.height();

        // Set the player width
        if (playerRatio > windowRatio) {
            $videoPlayer.css({
                width  : '100%',
                height : 'auto',
            });
        } else {
            $videoPlayer.css({
                height : '100%',
                width  : 'auto',
            });
        }

        // Get player width after resize
        var playerWidth  = $videoPlayer.width(),
            playerHeight = $videoPlayer.height();

        // Offset the player to center it in the page
        $videoPlayer.css('top', 0);
        $videoPlayer.css('left', 0);
        if (playerRatio > windowRatio) {
            var offsetH = Math.abs((playerHeight - $container.height()) / 2);
            $videoPlayer.css('top', offsetH + 'px');
            // this.$homeContainer.css('top', '-' + offsetH + 'px');
        } else {
            var offsetW = Math.abs((playerWidth - $container.width()) / 2);
            $videoPlayer.css('left', offsetW + 'px');
        }
    },

    /**
     * Hides the video when it hits the end
     * @return {void}
     */
    hideVideo : function (time) {
        // Disable the video centering
        clearInterval(this.resizeInterval);
        this.$window.off('resize');

        // Remove the video elements
        this.$stopVideo.remove();
        this.$toggleMute.remove();
        this.$videoPlayer[0].pause();

        this.$videoPlayer.width(this.$videoPlayer.width() + 'px');
        this.$videoPlayer.addClass('animated');

        // Show the 2nd video container and scroll to it
        this.$homeContainer.show();
        var scrollPosition = this.$homeContainer.position().top;
        $('html, body').animate({'scrollTop' : scrollPosition + 'px'}, time, function () {
            this.$videoPlayer.remove();
            // this.$homeContainer.css('top', 0);
            $('body').removeClass('home intro'); // Enable overflow, indicate the end of the intro
            this.$homeMenu.addClass('menu-visible');
        }.bind(this));
    },

    /**
     * Toggles the intro video sound
     * @param {Event} e
     * @return {void}
     */
    toggleIntroMute : function (e) {
        this.$videoPlayer[0].muted = !this.$videoPlayer[0].muted;
        if (this.$videoPlayer[0].muted) {
            $(e.currentTarget).text('volume_up')
        } else {
            $(e.currentTarget).text('volume_off')
        }
    },

    /**
     * Gets the device type
     * @return {string}
     */
    getDeviceType : function() {
        var deviceType = window.getComputedStyle(
            document.querySelector('body'), ':before'
        ).getPropertyValue('content');

        return deviceType.replace(/"/g, '');
    }
};
