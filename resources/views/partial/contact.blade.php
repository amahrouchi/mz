<div class="contact-container js-contact-container">
    <div class="contact-logo">
        <img src="/assets/img/logo.png" alt="Logo Hippographe">
    </div>
    <div class="contact-item contact-left">
        Contactez nous par téléphone<br>
        06.86.72.21.73
    </div>
    <div class="contact-item contact-right">
        Contactez nous par mail<br>
        design@hippograph.fr
    </div>
    <div class="contact-toggle js-contact-toggle">
        <i class="material-icons">mail</i>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.contact.init();
        });
    </script>
@endpush
