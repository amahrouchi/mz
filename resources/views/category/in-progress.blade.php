@extends('layouts.main', ['bodyClass' => 'in-progress'])

@section('title', 'Photos')

@section('content')
    <div class="category-container">
        @include('category.partial.header', [
            'category'      => $category,
            'allCategories' => $allCategories,
            'type'          => $type,
        ])

        <div class="in-progress-container">
           pAgE en cOnstrUctiOn
        </div>
    </div>
@endsection
