<?php

use App\Services\CategoryRepository;

return [
    'pro'     => [
        [
            'name'      => 'Éveil citoyen',
            'path'      => '/assets/videos/customers/eveil/1.mp4',
            'thumbnail' => '/assets/videos/customers/eveil/thumbnails/1.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_INSTITUTIONAL,
        ],
        [
            'name'      => 'Citoyen dès demain - Jingle',
            'path'      => '/assets/videos/other/pro/jingle-citoyens.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/jingle-citoyens.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'I feel so good - Kassidy',
            'path'      => '/assets/videos/other/pro/kassidi-i-feel-so-good.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/kassidi-i-feel-so-good.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'Palé ba yo - Kassidi',
            'path'      => '/assets/videos/other/pro/kassidi-pale-ba-yo.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/kassidi-pale-ba-yo.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'Goldy Lady - Ken Gazzy',
            'path'      => '/assets/videos/other/pro/ken-gazzy-goldy-lady.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/ken-gazzy-goldy-lady.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'Si c\'était hier - Ken Gazzy',
            'path'      => '/assets/videos/other/pro/ken-gazzy-si-c-etait-hier.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/ken-gazzy-si-c-etait-hier.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'Mon époque',
            'path'      => '/assets/videos/other/pro/mon-epoque.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/mon-epoque.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_CLIP,
        ],
        [
            'name'      => 'Institut Lumières - Présentation',
            'path'      => '/assets/videos/other/pro/presentation-institut-lumieres.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/presentation-institut-lumieres.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_INSTITUTIONAL,
        ],
        [
            'name'      => 'Teaser 5ème RAMR',
            'path'      => '/assets/videos/other/pro/teaser-5eme-ramr.mp4',
            'thumbnail' => '/assets/videos/other/pro/thumbnails/teaser-5eme-ramr.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_INSTITUTIONAL,
        ],
        [
            'name'      => 'Test 1',
            'path'      => '/assets/videos/other/1.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_LIVE,
        ],
        [
            'name'      => 'Test 2',
            'path'      => '/assets/videos/other/2.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_LIVE,
        ],
        [
            'name'      => 'Test 3',
            'path'      => '/assets/videos/other/3.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
            'subtype'   => CategoryRepository::VIDEO_SUBTYPE_LIVE,
        ],
    ],
    'private' => [
        [
            'name'      => 'Test 1',
            'path'      => '/assets/videos/other/1.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 2',
            'path'      => '/assets/videos/other/2.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 3',
            'path'      => '/assets/videos/other/3.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 4',
            'path'      => '/assets/videos/other/4.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 5',
            'path'      => '/assets/videos/other/5.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 6',
            'path'      => '/assets/videos/other/6.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
        [
            'name'      => 'Test 7',
            'path'      => '/assets/videos/other/7.mp4',
            'thumbnail' => '/assets/img/test/16-9.jpg',
        ],
    ],
];
