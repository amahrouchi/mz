<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page
Route::get('/', [HomeController::class, 'index'])
    ->name('home');

// Photo category
Route::get('/categorie/photos', [CategoryController::class, 'photos'])
    ->name('photo-category');

// Category page
Route::get('categorie/videos', [CategoryController::class, 'videos'])
     ->where('id', '[a-z-]+')
     ->name('video-category');

Route::get('categorie/{id}', [CategoryController::class, 'listCustomers'])
     ->where('id', '[a-z-]+')
     ->name('category');

// Customers page
Route::get('/client/{id}', [CustomerController::class, 'index'])
     ->where('id', '[a-z-]+')
     ->name('customer');
