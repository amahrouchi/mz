<?php


namespace App\Services;

/**
 * Class CategoryRepository
 * @package App\Services
 */
class CategoryRepository
{
    /**
     * Video category ID
     * @var string
     */
    public const VIDEO_CATEGORY_ID = 'videos';

    /**
     * Live type videos
     * @var string
     */
    public const VIDEO_SUBTYPE_LIVE = 'live';

    /**
     * Institutional type videos
     * @var string
     */
    public const VIDEO_SUBTYPE_INSTITUTIONAL = 'institutional';

    /**
     * Clip type videos
     * @var string
     */
    public const VIDEO_SUBTYPE_CLIP = 'clip';

    /**
     * Photo category ID
     * @var string
     */
    public const PHOTO_CATEGORY_ID = 'photos';

    /**
     * The categories data config path
     * @var string
     */
    private const CATEGORIE_PATH = 'data.categories';

    /**
     * The config path to get all video data
     * @var string
     */
    private const VIDEO_PATH = 'data.videos';

    /**
     * Finds a category by its ID
     * @param string $identifier
     * @return array|null
     */
    public function findById(string $identifier): ?array
    {
        $allCategories = config(self::CATEGORIE_PATH);
        return $allCategories[$identifier] ?? null;
    }

    /**
     * Finds all categories
     * @return array
     */
    public function findAll(): array
    {
        return config(self::CATEGORIE_PATH);;
    }

    /**
     * Find all video data
     * @return array
     */
    public function findAllVideos(): array
    {
        return config(self::VIDEO_PATH);
    }
}
