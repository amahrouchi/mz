<?php


namespace App\Http\Controllers;

use App\Services\CategoryRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Home page
     * @param CategoryRepository $categoryRepository
     * @return View
     */
    public function index(CategoryRepository $categoryRepository): View
    {
        // Handle the display of the intro video
        $noIntro = (bool)Cookie::get('no_intro', false);
        if ($noIntro === false) {
            Cookie::queue('no_intro', 1, 60);
        }

        $allCategories = $categoryRepository->findAll();

        return view('index', [
            'allCategories' => $allCategories,
            'disableIntro'  => $noIntro,
        ]);
    }
}
