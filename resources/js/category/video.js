var mzc      = mzc || {};
mzc.category = mzc.category || {};

/**
 * Category page JS module
 * @type {{}}
 */
mzc.category.video = {

    /**
     * The video toggle container selector
     * @type {String}
     */
    videoToggleContainerSelector : '.js-video-toggle-container',

    /**
     * The element used to toggle pro/private videos
     * @type {String}
     */
    videoToggleSelector : '.js-video-toggle',

    /**
     * The subtypes selection menu
     * @type {string}
     */
    subTypeSelector : '.js-subtype',

    /**
     * The video category content container
     * @type {jQuery}
     */
    $videoCategoryContent : $('.js-video-category-content'),

    /**
     * The JS thumbnails
     * @type {jQuery}
     */
    $videoThumbnails : $('.js-video-thumbnails'),

    /**
     * The thumbnail item elements
     * @type {jQuery}
     */
    $thumbnailItems : $('.js-thumbnail-item'),

    /**
     * The video player element
     * @type {jQuery}
     */
    $videoPlayer : $('.js-video-player'),


    /**
     * Inits the video category module
     * @return {void}
     */
    init : function () {
        $(this.videoToggleSelector).click(this.toggleVideos.bind(this));
        this.$thumbnailItems.click(this.playVideo.bind(this));

        $(this.subTypeSelector).click(this.filterSubType.bind(this));

        mzc.index.$window.resize(this.resizeList.bind(this));
        this.resizeList();
    },

    /**
     * Toggles the thumbnail display
     * @param {Event} e
     * @return {void}
     */
    toggleVideos : function (e) {
        e.preventDefault();

        // Filter the video list
        var target = $(e.currentTarget).data('target');
        this.$thumbnailItems.filter('[data-video-type=' + target + ']').show();
        this.$thumbnailItems.filter(':not([data-video-type=' + target + '])').hide();

        // Load the fisrt video
        this.loadFirstVideo();

        // Toggle the container state
        $(this.videoToggleContainerSelector).removeClass('active');
        $(e.currentTarget).closest(this.videoToggleContainerSelector).addClass('active');

        // Reset the subtype display
        $(this.subTypeSelector).removeClass('active');
    },

    /**
     * Filter by video subtype
     * @param {Event} e
     * @return {void}
     */
    filterSubType : function (e) {
        e.preventDefault();
        e.stopPropagation();

        // Init target and subtype
        var target = $(e.currentTarget).closest(this.videoToggleContainerSelector)
                                       .find(this.videoToggleSelector)
                                       .data('target');
        var subType = $(e.currentTarget).data('subtype');

        // Init the subtype display
        $(this.subTypeSelector).removeClass('active');
        $(e.currentTarget).addClass('active');

        // Filter the video list
        this.$thumbnailItems.filter('[data-video-type=' + target + '][data-video-subtype=' + subType + ']').show();
        this.$thumbnailItems.filter(':not([data-video-type=' + target + '][data-video-subtype=' + subType + '])').hide();

        // Load the 1st video
        this.loadFirstVideo();
    },

    /**
     * Loads the first video of available
     * @return {void}
     */
    loadFirstVideo: function () {
        var $video = this.$thumbnailItems.filter(':visible').first();
        if ($video.length) {
            this.loadVideo($video);
        }
    },

    /**
     * Play the selected video
     * @param {Event} e
     * @return {void}
     */
    playVideo : function (e) {

        var $currElt  = $(e.currentTarget);

        // Update the playing video
        this.loadVideo($currElt);
        this.$videoPlayer[0].play();

        // Scroll to the video position
        this.scrollToVideo();
    },

    /**
     * Loads the first visible video into the player
     * @param {jQuery} $video
     * @return {void}
     */
    loadVideo($video) {
        var thumbnail = $video.data('videoThumbnail');
        var path      = $video.data('videoPath');

        this.$videoPlayer[0].pause();
        this.$videoPlayer.find('source').remove();
        this.$videoPlayer.append('<source src="' + path + '" type="video/mp4">');
        this.$videoPlayer.attr('poster', thumbnail);
        this.$videoPlayer[0].load()
    },

    /**
     * Scroll to the video position
     * @return {void}
     */
    scrollToVideo : function () {
        var offset = this.$videoCategoryContent.offset();
        $('html, body').animate({'scrollTop' : offset.top + 'px'}, 500);
    },

    /**
     * Adjust the player video list height
     * @return {void}
     */
    resizeList : function () {
        var deviceType = mzc.index.getDeviceType();
        if (deviceType !== 'desktop') {
            this.$videoThumbnails.removeAttr('style');
            return;
        }

        var playerHeight = this.$videoPlayer.height();
        this.$videoThumbnails.css('height', playerHeight + 'px');
    }
};
