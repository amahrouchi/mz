@extends('layouts.main', ['bodyClass' => 'category videos'])

@section('title', 'Videos')

@section('content')

    <div class="category-container">

        @include('category.partial.header', [
            'category'      => $category,
            'allCategories' => $allCategories,
            'type'          => $type,
        ])

        <div class="video-category-content js-video-category-content">

            <div class="video-types">
                <ul class="main-types">
                    <li class="js-video-toggle-container active">
                        <a href="#" class="js-video-toggle" data-target="pro">
                            Professionnels
                            <i class="material-icons">expand_more</i>
                        </a>

                        <ul class="subtypes">
                            <li>
                                <a href="#" data-subtype="{{ $institutionalSubType }}" class="js-subtype">Institutionelle</a>
                            </li>
                            <li>
                                <a href="#" data-subtype="{{ $liveSubType }}" class="js-subtype">Live</a>
                            </li>
                            <li>
                                <a href="#" data-subtype="{{ $clipSubType }}" class="js-subtype">Clip</a>
                            </li>
                        </ul>
                    </li>
                    <li class="js-video-toggle-container">
                        <a href="#" class="js-video-toggle" data-target="private">Particuliers</a>
                    </li>
                </ul>
            </div>

            <div class="video-category-inner-content">

                <div class="video-category-player">
                    <video class="js-video-player"
                           preload="auto"
                           poster="{{ $videos['pro'][0]['thumbnail'] }}"
                           controls
                           controlsList="nodownload"
                    >
                        <source src="{{ $videos['pro'][0]['path'] }}" type="video/mp4">
                    </video>
                </div>

                <div class="video-thumbnails js-video-thumbnails">

                    @foreach($videos['pro'] as $video)
                        <div class="thumbnail-item js-thumbnail-item"
                             data-video-type="pro"
                             data-video-subtype="{{ $video['subtype'] }}"
                             data-video-thumbnail="{{ $video['thumbnail'] }}"
                             data-video-path="{{ $video['path'] }}">
                            <img src="{{ $video['thumbnail'] }}" alt="{{ $video['name'] }}">
                            <div class="video-description">
                                <i class="material-icons">play_arrow</i>
                                <div class="video-title">{{ $video['name'] }}</div>
                            </div>
                        </div>
                    @endforeach

                    @foreach($videos['private'] as $video)
                        <div class="thumbnail-item private-video js-thumbnail-item"
                             data-video-type="private"
                             data-video-thumbnail="{{ $video['thumbnail'] }}"
                             data-video-path="{{ $video['path'] }}">
                            <img src="{{ $video['thumbnail'] }}" alt="{{ $video['name'] }}">
                            <div class="video-description">
                                <i class="material-icons">play_arrow</i>
                                <div class="video-title">{{ $video['name'] }}</div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.category.video.init();
        });
    </script>
@endpush
