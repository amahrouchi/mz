@extends('layouts.main', ['bodyClass' => 'category'])

@section('title', $category['name'])

@section('content')

    <div class="category-container">
        @include('category.partial.header', [
            'category'      => $category,
            'allCategories' => $allCategories,
        ])

        <div class="category-content">
            <div class="category-nav">
                <div class="nav-item prev js-nav-item"><i class="material-icons">navigate_before</i></div>
                <div class="nav-item next js-nav-item"><i class="material-icons">navigate_next</i></div>
            </div>

            <div class="category-customer-list js-category-customer-list">
                @php ($count = 0)
                @foreach ($customers as $idCustomer => $customer)
                    <div class="category-item {{ $idCustomer }} js-category-item"
                         style="background-image: url({{ $customer['backgroundImage'] }});left: {{ 100 * $count }}vw">
                        <div class="customer-description">
                            <a href="{{ route('customer', ['id' => $idCustomer]) }}"
                               class="customer-description-content js-customer-description-content js-{{ $idCustomer }}-toggle">
                                <div class="name">
                                    {!! $customer['htmlName'] ?? $customer['name'] !!}
                                </div>
                                <div class="description">{{ $customer['description'] ?? '' }}</div>
                                <span class="see-more">Voir</span>
                            </a>
                        </div>

                        <div class="customer-image js-customer-image">
                            <a href="{{ route('customer', ['id' => $idCustomer]) }}" class="js-{{ $idCustomer }}-toggle">
                                <img src="{{ $customer['thumbnailImage'] }}" alt="{{ $customer['name'] }}">
                            </a>
                        </div>

                        @if($idCustomer === 'logos')
                            <div class="logo-slider js-logo-slider">
                                @foreach($customer['logos'] as $i => $logo)
                                    <img class="logo-image {{ $logo['id'] }}"
                                         src="{{ $logo['path'] }}"
                                         alt="Logo {{ $logo['id'] }}">
                                @endforeach

                                <div class="logo-slider-drag js-logo-slider-drag" draggable="true"></div>
                            </div>
                        @endif

                    </div>
                    @php ($count++)
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.category.init();
        });
    </script>
@endpush

