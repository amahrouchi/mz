var mzc = mzc || {};

/**
 * Main menu JS script
 * @type {{}}
 */
mzc.mainMenu = {

    /**
     * The menu jQuery element
     * @var {jQuery}
     */
    $mainMenu : null,

    /**
     * Selector used to gather menu toggle buttons
     * @type {string}
     */
    toggleMenuSelector : '.js-toggle-menu',

    /**
     * Submenu toggle selector
     * @type {string}
     */
    subMenuToggleSelector : '.js-has-submenu > a',

    /**
     * Submenu selector
     * @type {string}
     */
    subMenuSelector : '.js-has-submenu',

    /**
     * Module initializer
     * @return {void}
     */
    init : function () {
        // Init the main menu element
        this.$mainMenu = $('.js-main-menu');

        // Init the menu display behaviour
        $(this.toggleMenuSelector).on('click', this.toggleMenu.bind(this));

        $(this.subMenuToggleSelector).on('click', this.toggleSubMenu.bind(this));
    },

    /**
     * Toggles the menu display
     * @return {void}
     */
    toggleMenu : function () {
        this.$mainMenu.toggleClass('main-menu-visible');
    },

    toggleSubMenu : function (e) {
        e.preventDefault();
        $(e.currentTarget).parent(this.subMenuSelector).toggleClass('opened');
    }
};
