<?php

namespace App\Providers;

use App\Services\CategoryRepository;
use App\Services\CustomerRepository;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Pass the categories to the layout
        view()->composer('layouts.main', function ($view): void {
            /** @var CategoryRepository $categoryRepo */
            $categoryRepo = app(CategoryRepository::class);
            $categories = $categoryRepo->findAll();
            $view->with('categories', $categories);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomerRepository::class, function (Application $app) {
            return new CustomerRepository();
        });

        $this->app->singleton(CategoryRepository::class, function (Application $app) {
            return new CategoryRepository();
        });
    }
}
