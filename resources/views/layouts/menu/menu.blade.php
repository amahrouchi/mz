<i class="material-icons open-menu white js-toggle-menu">menu</i>
<nav class="main-menu js-main-menu">
    <i class="close-menu material-icons js-toggle-menu">close</i>

    <ul class="menu-items">
        <li class="menu-item item-1">
            <a href="{{ route('home') }}">Accueil</a>
        </li>
        <li class="menu-item item-2 has-submenu js-has-submenu">
            <a href="#">Catégories</a>
            <ul class="submenu">
                @foreach ($categories as $category)
                    <li class="submenu-item">
                        <a href="{{ route('category', ['id' => $category['id']]) }}">{{ $category['name'] }}</a>
                    </li>
                @endforeach
            </ul>
        </li>
        <li class="menu-item item-3">
            <a href="#">Services</a>
        </li>
        <li class="menu-item item-4">
            <a href="#">À propos</a>
        </li>
        <li class="menu-item item-5">
            <a href="#">Il nous ont fait confiance</a>
        </li>
        <li class="menu-item item-6">
            <a href="#">Avis de clients</a>
        </li>
        <li class="menu-item item-7">
            <a href="#" class="js-contact-toggle">Contact</a>
        </li>
    </ul>
</nav>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.mainMenu.init();
        });
    </script>
@endpush
