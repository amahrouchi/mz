@php ($bodyClass = $disableIntro ? 'no-intro' : 'home intro')
@extends('layouts.main', ['bodyClass' => $bodyClass])

@section('title', 'Accueil')

@section('content')

    <video class="video-player js-video-player" muted>
        <source src="/assets/videos/intro.mp4" type="video/mp4">
    </video>
    <div class="stop-video js-stop-video">Ignorer l'intro</div>
    <i class="material-icons toggle-mute js-toggle-mute">volume_up</i>

    <div class="home-container js-home-container">

        <div class="screen-container js-screen-container">
            <img src="/assets/img/screen.png" alt="Video screen" class="video-screen">

            <div class="video-subcontainer">
                <video preload="auto"
                       poster="assets/img/logo-2.png"
                       controls controlsList="nodownload"
                >
                    <source src="/assets/videos/works.mp4" type="video/mp4">
                </video>
            </div>
        </div>

        <ul class="home-menu js-home-menu">
            @php ($count = 1)
            @foreach ($allCategories as $id => $category)
                <li class="menu-item item-{{ $count }}"><a href="{{ route('category', ['id' => $id]) }}">{{ $category['name'] }}</a></li>
                @php ($count++)
            @endforeach

            <li class="menu-item item-{{ $count }}">
                <a href="#" class="js-contact-toggle">Contact</a>
            </li>
        </ul>
        
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            mzc.index.init();
        });
    </script>
@endpush
