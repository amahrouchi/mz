var mzc = mzc || {};

/**
 * Customer page script
 * @type {{}}
 */
mzc.customer = {

    /**
     * The current scroll position
     * @type {int}
     */
    scrollPosition : $(window).scrollTop(),

    /**
     * Customer item selector
     * @type {string}
     */
    customerItem : '.js-customer-item',

    /**
     * Current customer item selector
     * @type {string}
     */
    currentCustomerItem : '.js-current',

    /**
     * The customer nav item selector
     * @type {string}
     */
    navItem : '.js-customer-nav-item',

    /**
     * The nav arrow selector
     * @type {string}
     */
    navNext : '.js-nav-item',

    /**
     * Slider item selector
     * @type {String}
     */
    slider: '.js-slider',

    /**
     * Slider items selector
     * @type {String}
     */
    sliderItem : '.js-slider-item',

    /**
     * Video container selector
     * @type {String}
     */
    video : '.js-video',

    /**
     * The slider bar selector
     * @type {String}
     */
    sliderBar : '.js-slider-bar',

    /**
     * Current slider element
     * @type {String}
     */
    currentSliderItemClass : 'js-slider-current',

    /**
     * Whether the scroll animation is currently running
     * @type {boolean}
     */
    animating : false,

    /**
     * Interval of time between 2 slider items
     * @type {int}
     */
    sliderInterval : 4000,

    /**
     * Initializes the module
     * @return void
     */
    init : function () {
        // Global navigation
        // this.initScroll(); // Let's keep we never know...
        $(this.navItem).click(this.clickNavItem.bind(this));

        // Slider
        $(this.slider).on('slider-next', this.sliderNext.bind(this));
        this.initSlider();

        // Videos
        this.initVideos();
    },

    /**
     * Initializes the "item per item" scroll effect
     * @return {void}
     */
    initScroll : function () {
        this.animating = true;
        this.scrollTo(0);

        $(window).on('scroll', function (e) {

            // Prevent multiple scrolling events
            if (this.animating) {
                return;
            }
            this.animating = true;


            // Init scrolling
            var nextScrollPosition;
            var scrollPosition = $(window).scrollTop();
            var $currentItem   = $(this.currentCustomerItem);

            // Scroll down
            if (scrollPosition >= this.scrollPosition) {
                if ($currentItem.next(this.customerItem).length === 0) {
                    this.animating = false;
                    return;
                }

                nextScrollPosition = $currentItem.next(this.customerItem).position().top;
                $currentItem.removeClass('js-current');
                $currentItem.next(this.customerItem).addClass('js-current');
            }
            // Scroll up
            else {

                if ($currentItem.prev(this.customerItem).length === 0) {
                    this.animating = false;
                    return;
                }

                nextScrollPosition = $currentItem.prev(this.customerItem).position().top;
                $currentItem.removeClass('js-current');
                $currentItem.prev(this.customerItem).addClass('js-current');
            }

            // Animate to next image
            this.scrollTo(nextScrollPosition);
        }.bind(this));
    },

    /**
     * Scroll to the selected nav item
     * @return {void}
     */
    clickNavItem : function (e) {
        e.preventDefault();

        // Init target info
        var targetId = $(e.currentTarget).data('target');
        var targetPosition = $(this.customerItem + '[data-id=' + targetId + ']').position().top;

        // Move the "js-current" class
        var $currentItem   = $(this.currentCustomerItem);
        $currentItem.removeClass('js-current');
        $(this.customerItem + '[data-id=' + targetId + ']').addClass('js-current');

        // Scroll
        this.animating      = true;
        this.scrollTo(targetPosition);
    },

    /**
     * Scroll to a specific position
     * @param position
     */
    scrollTo : function (position) {
        $('body, html').animate({'scrollTop' : position}, 1000, function () {
            setTimeout(function () {
                this.animating      = false;
                this.scrollPosition = position;
            }.bind(this), 100);
        }.bind(this));
    },

    /**
     * Initializes the slider
     * @return {void}
     */
    initSlider : function () {

        // Place items properly
        $(window).resize(function () {
            var sliderWidth = $(this.slider).width();
            var sliderItemWidth       = $(this.slider).find(this.sliderItem + ':first').width();
            $(this.slider).find(this.sliderItem).css(
                'left',
                ((sliderWidth - sliderItemWidth) / 2) + 'px'
            );
        }.bind(this));
        $(window).resize();

        // Auto-navigate in the slider
        this.sliderProgressBar();
        setInterval(function () {
            $(this.slider).trigger('slider-next');
            this.sliderProgressBar();
        }.bind(this), this.sliderInterval);
        $(this.slider).trigger('slider-next');

    },

    /**
     * Inits the slider progress bar
     * @return {void}
     */
    sliderProgressBar : function () {
        $(this.sliderBar)
            .width(0)
            .animate(
                {'width' : '100%'},
                this.sliderInterval - 100
            );
    },

    /**
     * Show the next item in the slider
     * @param {Event} e
     */
    sliderNext : function (e) {
        var $slider = $(e.currentTarget);

        // Define the current item
        var $currItem = $slider.find('.' + this.currentSliderItemClass);
        if ($currItem.length === 0) {
            $slider.find(this.sliderItem + ':first')
                   .addClass(this.currentSliderItemClass)
                   .show();
            return;
        }

        // Define the next item
        var $nextItem = $currItem.next(this.sliderItem);
        if ($nextItem.length === 0) {
            $nextItem = $slider.find(this.sliderItem + ':first');
        }

        // Hide current, show next
        $currItem.removeClass(this.currentSliderItemClass).fadeOut(1000);
        $nextItem.addClass(this.currentSliderItemClass)
                 .fadeIn(1000);
    },

    /**
     * Initializes video containers
     * @return {void}
     */
    initVideos : function () {
        $(this.video).each(function (i, videoContainer) {
            var $videoContainer = $(videoContainer);
            setInterval(
                mzc.index.centerVideo.bind(null, $videoContainer.find('video'), $videoContainer),
                500
            )
        });
    }
};
