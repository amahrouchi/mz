const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// CSS vendor
// mix.combine([
// ], 'public/css/vendor.css');

// JS vendor
mix.combine([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/lodash/lodash.min.js'
], 'public/js/vendor.js');



// Compile LESS files
mix.less('resources/less/app.less', 'public/css');


// Concat JS files
mix.combine([
    'resources/js/**/*.js',
], 'public/js/app.js');

// Source maps & files versioning
mix.sourceMaps()
   .version();
